if exists('g:no_python_conceal') || !has('conceal') || &enc != 'utf-8'
  finish
endif

syntax clear pythonOperator

syn match pyNiceOperator	"==" conceal cchar=≡
syn match pyNiceOperator	"!=" conceal cchar=≢
syn match pyNiceOperator	"<=" conceal cchar=≤
syn match pyNiceOperator	">=" conceal cchar=≥

syn match pyNiceOperator	"\<in\>" conceal cchar=∈
syn match pyNiceOperator	"\<or\>" conceal cchar=∨
syn match pyNiceOperator	"\<not " conceal cchar=¬
syn match pyNiceOperator	"\<and\>" conceal cchar=∧
syn match pyNiceOperator	"\<not in\>" conceal cchar=∉

syn match pyNiceKeyword		"\<\%(math\.\)\?pi\>" conceal cchar=π
syn match pyNiceOperator	"\<\%(math\.\)\?sqrt\>" conceal cchar=√
syn match pyNiceOperator	"\<\%(math\.\|\)ceil\>" conceal cchar=⌈
syn match pyNiceOperator	"\<\%(math\.\|\)floor\>" conceal cchar=⌊

syn keyword pyNiceOperator	sum conceal cchar=∑
syn keyword pyNiceStatement	int conceal cchar=ℤ
syn keyword pyNiceStatement	float conceal cchar=ℝ
syn keyword pyNiceStatement	complex conceal cchar=ℂ

syn keyword pyNiceStatement	lambda conceal cchar=λ

hi link pyNiceKeyword		Keyword
hi link pyNiceOperator		Operator
hi link pyNiceStatement		Statement

hi! link Conceal Operator

setlocal conceallevel=1
